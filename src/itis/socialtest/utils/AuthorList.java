package itis.socialtest.utils;

import itis.socialtest.entities.Author;

import java.util.ArrayList;

public class AuthorList{
    public ArrayList<Author> authorList = new ArrayList<>();

    public Author getBuId(Long id){
        for (int i = 0; i < authorList.size(); i++){
            if (authorList.get(i).getId() == id){
                return authorList.get(i);
            }
        }
        return null;
    }
}
