package itis.socialtest;


import itis.socialtest.entities.Post;
import itis.socialtest.utils.AuthorList;

import java.util.List;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие опреации:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private List<Post> allPosts;

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public static void main(String[] args) {
        new MainClass().run("src/itis/socialtest/resources/PostDatabase.csv", "src/itis/socialtest/resources/Authors.csv");
    }

    private void run(String postsSourcePath, String authorsSourcePath) {
        AuthorList authors = AuthorParser.parseAuthors(authorsSourcePath);
        allPosts = PostParser.parsePosts(postsSourcePath, authors);

        printPosts(allPosts);
        System.out.println("----------------");


        AnalyticsService analyticsService = new AnalyticsServiceImpl();
        analyticsService.findPostsByDate(allPosts, "17.04.2021T10:00");
        List<Post> nickPosts = analyticsService.findAllPostsByAuthorNickname(allPosts, "Depresnyasha");
        System.out.println(nickPosts.size());
        printPosts(nickPosts);
        System.out.println(analyticsService.checkPostsThatContainsSearchString(allPosts, "Обычно"));
        analyticsService.findMostPopularAuthorNickname(allPosts);

    }

    private void printPosts(List<Post> posts){
        for (Post post: posts){
            System.out.println("Author: " + post.getAuthor().getNickname() + "\n" +
                    "Content: " + post.getContent() + "" +
                    "\nDate:  " + post.getDate() +
                    "\nCount: " + post.getLikesCount());
            System.out.println();
        }

    }
}
