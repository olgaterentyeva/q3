package itis.socialtest;

import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AnalyticsServiceImpl implements AnalyticsService {
    @Override
    public List<Post> findPostsByDate(List<Post> posts, String date) {
        return posts.stream().filter(post -> post.getDate().equals(date)).collect(Collectors.toList());
    }

    @Override
    public String findMostPopularAuthorNickname(List<Post> posts) {
        Map<Author, Long> map = posts.stream().collect(
                Collectors.groupingBy(Post::getAuthor, Collectors.summingLong(Post::getLikesCount)));
        Long max = 0L;
        String maxValue = "";
        for (Author value : map.keySet()) {
            if (map.get(value) > max) {
                max = map.get(value);
                maxValue = value.getNickname();
            }
        }
        return maxValue;
    }

    @Override
    public Boolean checkPostsThatContainsSearchString(List<Post> posts, String searchString) {
        return posts.stream()
                .allMatch(post -> post.getContent().equals(searchString));
    }

    @Override
    public List<Post> findAllPostsByAuthorNickname(List<Post> posts, String nick) {
        return posts.stream()
                .filter(post -> post.getAuthor().equals(nick)).collect(Collectors.toList());
    }

}
