package itis.socialtest;

import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;
import itis.socialtest.utils.AuthorList;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PostParser {
    public static List<Post> parsePosts(String path, AuthorList authors) {
        List<Post> posts = new ArrayList<>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(path));
            while (bufferedReader.ready()) {
                String line = bufferedReader.readLine();
                String[] lines = line.split(", ");
                Long id = Long.valueOf(lines[0]);
                Long likesCount = Long.valueOf(lines[1]);
                String date = lines[2];
                String content = lines[3];
                Author author = authors.getBuId(id);
                Post post = new Post(date, content, likesCount, author);
                posts.add(post);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return posts;
    }
}

