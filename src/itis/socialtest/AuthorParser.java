package itis.socialtest;

import itis.socialtest.utils.AuthorList;
import itis.socialtest.entities.Author;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class AuthorParser {
    public static AuthorList parseAuthors(String path) {
        itis.socialtest.utils.AuthorList authors = new itis.socialtest.utils.AuthorList();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(path));
            while (bufferedReader.ready()) {
                String line = bufferedReader.readLine();
                String[] lines = line.split(", ");
                Long id = Long.valueOf(lines[0]);
                String nickname = lines[1];
                String birthday = lines[2];
                Author author = new Author(id, nickname, birthday);
                authors.authorList.add(author);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return authors;
    }
}

